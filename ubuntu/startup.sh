#!/bin/sh

if ! grep 'GOPATH' $HOME/.bashrc;
then
  mkdir $HOME/go
  sh -c "echo 'export GOPATH=$HOME/go' >> $HOME/.bashrc"
  sh -c "echo 'export PATH=$PATH:/usr/local/go/bin:$GOPATH/bin' >> $HOME/.bashrc"
#  source $HOME/.bashrc
  exec $SHELL -l
fi


#cd home/vagrant
#docker-compose up -d 1>&2
