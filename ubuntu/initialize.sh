#!/bin/sh

apt -y update
# docker-compose
sh -c "curl -L https://github.com/docker/compose/releases/download/1.15.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose"
sh -c "chmod +x /usr/local/bin/docker-compose"

# Go言語（https://golang.org/dl/）
if uname -m | grep 'x86_64';
then
	curl https://storage.googleapis.com/golang/go1.9.1.linux-amd64.tar.gz > /tmp/go1.9.1.linux.tar.gz  #64bit版の人はこちら
else
	curl https://storage.googleapis.com/golang/go1.9.1.linux-386.tar.gz > /tmp/go1.9.1.linux.tar.gz  #32bit版の人はこちら
fi

cd /tmp && tar zxvf go1.9.1.linux.tar.gz
mv go /usr/local/go1.9
ln -s /usr/local/go1.9 /usr/local/go

# node.js
apt install -y nodejs npm
npm cache clean
npm install n -g
n stable
ln -sf /usr/local/bin/node /usr/bin/node
apt purge -y nodejs npm
#n 6.11.2
n 6.9.5

#npm install npm@3.10.10 -g
npm install --global grpc


# git(ver 2.*.*)
apt install -y git
add-apt-repository -y ppa:git-core/ppa
apt -y update
#apt -y upgrade
