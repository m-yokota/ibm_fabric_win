/**
 * New script file
 * @param {org.acme.bidgame.Bid} bid
 * @transaction
 */

function makeBid(bid) {
  var stage = bid.stage;
  
  //todo: 持ち点がない場合参加できないようにする処理
  
  if (stage.bids == null){
   stage.bids = [] 
  }

  stage.bids.push(bid);
  
  return getAssetRegistry('org.acme.bidgame.Stage')
  	.then(function(stageRegistry){
    	return stageRegistry.update(stage);
  });

}

/**
 * New script file
 * @param {org.acme.bidgame.CloseBidding} closeBidding
 * @transaction
 */
function closeBidding(closeBidding){
  var stage = closeBidding.stage;
  
  var highestPoint = null;
  var lowestPoint = null;
  var basicPoint = null;
  
  var players = []
  
  if(stage.bids && stage.bids.length > 0){
    
   stage.bids.sort(function(a, b){
    return (b.bidPoint - a.bidPoint); 
   });
  
    basicPoint = stage.basicPoint;
  	highestPoint = stage.bids[0].bidPoint;
    lowestPoint = stage.bids[stage.bids.length - 1].bidPoint;
  
   for(var elem in stage.bids){
     if(highestPoint == stage.bids[elem].bidPoint){
       stage.bids[elem].player.point += basicPoint - highestPoint;
     }
     if(lowestPoint == stage.bids[elem].bidPoint){
       stage.bids[elem].player.point += -basicPoint - lowestPoint;
     }
     players.push(stage.bids[elem].player);
   }


  }
  

  //todo: StageをCloseする処理
  //todo: 脱落者を通知する処理
  //todo: 優勝者を通知する処理
  
	return getParticipantRegistry('org.acme.bidgame.Player')
  .then(function(playerRegistry){
      return playerRegistry.updateAll(players);
    })
  
  ;
  
  
  
}
