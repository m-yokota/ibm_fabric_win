#/bin/bash

sed -e 's/^ExecStart.*/ExecStart=\/usr\/bin\/dockerd -H fd:\/\/ --cluster-store=consul:\/\/192.168.33.1:8500 --cluster-advertise=enp0s8:2376/g' /lib/systemd/system/docker.service -i
systemctl daemon-reload
systemctl restart docker
