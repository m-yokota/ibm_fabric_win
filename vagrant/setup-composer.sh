#!/bin/sh

docker ps -aq | xargs docker rm -f
docker images -aq | xargs docker rmi -f
curl -sSL https://hyperledger.github.io/composer/install-hlfv1.sh | bash

sudo curl -O https://hyperledger.github.io/composer/prereqs-ubuntu.sh
sudo chmod u+x prereqs-ubuntu.sh
sudo echo `./prereqs-ubuntu.sh`
sudo npm install -g composer-cli
sudo npm install -g generator-hyperledger-composer
sudo npm install -g composer-rest-server
sudo npm install -g yo
sudo npm install -g composer-playground
