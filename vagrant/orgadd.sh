#!/bin/sh

cp /vagrant/docker-compose-base.yaml $HOME/fabric-samples/first-network/base/
cp /vagrant/crypto-config.yaml $HOME/fabric-samples/first-network/
cp /vagrant/docker-compose-cli.yaml $HOME/fabric-samples/first-network/
cp /vagrant/docker-compose-e2e.yaml $HOME/fabric-samples/first-network/
cp /vagrant/docker-compose-e2e-template.yaml $HOME/fabric-samples/first-network/
cp /vagrant/_byfn.sh $HOME/fabric-samples/first-network/
cp /vagrant/_script.sh $HOME/fabric-samples/first-network/scripts/
cp /vagrant/chaincode_example02.go $HOME/fabric-samples/chaincode/chaincode_example02/
cp /vagrant/setGlobals.sh $HOME/fabric-samples/first-network/scripts/

chmod +x $HOME/fabric-samples/first-network/_byfn.sh
chmod +x $HOME/fabric-samples/first-network/scripts/_script.sh
chmod +x $HOME/fabric-samples/first-network/scripts/setGlobals.sh
