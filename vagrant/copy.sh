#!/bin/sh

cp ./docker-compose-cli.yaml $HOME/fabric-samples/first-network/
cp ./configtx.yaml $HOME/fabric-samples/first-network/
cp ./crypto-config.yaml $HOME/fabric-samples/first-network/
cp ./docker-compose-e2e-template.yaml $HOME/fabric-samples/first-network/
cp ./docker-compose-base.yaml $HOME/fabric-samples/first-network/base/
cp ./peer-base.yaml $HOME/fabric-samples/first-network/base/

cp ./byfn.sh $HOME/fabric-samples/first-network/
cp ./_script.sh $HOME/fabric-samples/first-network/scripts/

cp ./docker-compose-cli-add-template.yaml $HOME/fabric-samples/first-network/

mkdir -p $HOME/fabric-samples/chaincode/chaincode_example_h2903b
cp ./chaincode_example_h2903b.go $HOME/fabric-samples/chaincode/chaincode_example_h2903b/

