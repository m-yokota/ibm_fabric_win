#!/bin/sh

if ! grep 'fabric-samples' $HOME/.bashrc;
then
  cd $HOME
  git clone https://github.com/hyperledger/fabric-samples.git
  cd fabric-samples
  curl -sSL https://goo.gl/fMh2s3 | bash
  sh -c "echo 'export PATH=$HOME/fabric-samples/bin:$PATH' >> $HOME/.bashrc"
  cd $HOME
  exec $SHELL -l
fi

